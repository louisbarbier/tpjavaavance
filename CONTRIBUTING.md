# Welcome to the contributing guide

## Bring contribution
If you want to contribute on a feature you should create an issue on the gitlab project. Then, create a feature branch from the issue.

Submit merge request, and wait for validation.

## Code
Please respect clean code strategy. In english. 
KISS principe.

## Git usage
### Git template
```sh
git commit -m "prefix: My simple description"
```
prefix:
- feat: Adding new feature
- fix: Fixing a bug on a feature
- test: Adding test
- doc: Adding javadoc
- project: Updating project documentation


